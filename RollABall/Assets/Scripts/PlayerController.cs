using UnityEngine;

// Include the namespace required to use Unity UI and Input System
using UnityEngine.InputSystem;
using TMPro;

public class PlayerController : MonoBehaviour
{

	// Create public variables for player speed, and for the Text UI game objects
	public float speed;
	public TextMeshProUGUI countText;
	public GameObject winTextObject;

	[SerializeField] LayerMask groundedMask;
	[SerializeField] float jumpForce;
	[SerializeField] float maxSpeed;
	[SerializeField] Material activeMat;
	[SerializeField] Material inactiveMat;
	private GameObject checkpoint;
	private Vector3 startingPos;
	private float movementX;
	private float movementY;

	private Rigidbody rb;
	private int count;

	private bool grounded = false;

	// At the start of the game..
	void Start()
	{
		// Assign the Rigidbody component to our private rb variable
		rb = GetComponent<Rigidbody>();

		// Set the count to zero 
		count = 0;

		SetCountText();

		// Set the text property of the Win Text UI to an empty string, making the 'You Win' (game over message) blank
		winTextObject.SetActive(false);

		startingPos = transform.position;
	}

	void FixedUpdate()
	{
		// Create a Vector3 variable, and assign X and Z to feature the horizontal and vertical float variables above
		Vector3 movement = new Vector3(movementX, 0.0f, movementY);

		rb.AddForce(movement * speed);

		if(rb.velocity.magnitude > maxSpeed)
        {
			rb.velocity = rb.velocity.normalized * maxSpeed;
        }
		Collider[] hits = Physics.OverlapSphere(transform.position - new Vector3(0, .5f), .1f, groundedMask);
		if(hits.Length > 0)
        {
			grounded = true;
        }
        else
        {
			grounded = false;
        }
	}

	void OnTriggerEnter(Collider other)
	{
		Debug.Log(other.tag);
		// ..and if the GameObject you intersect has the tag 'Pick Up' assigned to it..
		if (other.gameObject.CompareTag("PickUp"))
		{
			other.gameObject.SetActive(false);

			// Add one to the score variable 'count'
			count = count + 1;

			// Run the 'SetCountText()' function (see below)
			SetCountText();
		}
		else if(other.gameObject.CompareTag("Reset"))
        {
			Debug.Log(other.tag);
			Reset();
        }
		else if(other.gameObject.CompareTag("Checkpoint"))
        {
			Checkpoint(other.gameObject);
        }
	}

	void OnMove(InputValue value)
	{
		Vector2 v = value.Get<Vector2>();

		movementX = v.x;
		movementY = v.y;
	}

	void OnJump()
    {
		if(grounded)
        {
			rb.AddForce(new Vector3(0, 1) * jumpForce);
		}
    }

	void SetCountText()
	{
		countText.text = "Count: " + count.ToString();
	}

    private void OnDrawGizmos()
    {
		Gizmos.DrawWireSphere(transform.position - new Vector3(0, .5f), .1f);
    }

    public void Reset()
    {
		
		if (checkpoint != null)
        {
			transform.position = checkpoint.transform.position;
        }
        else
        {
			transform.position = startingPos;
        }
		rb.velocity = Vector3.zero;
    }

	public void Checkpoint(GameObject point)
    {
		if(checkpoint != null)
        {
			checkpoint.GetComponent<MeshRenderer>().material = inactiveMat;
		}
		checkpoint = point;
		point.GetComponent<MeshRenderer>().material = activeMat;

		if(point.name == "End")
        {
			winTextObject.SetActive(true);
		}
    }

}
